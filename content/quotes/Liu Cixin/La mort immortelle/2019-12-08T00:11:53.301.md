+++
author = ["Alexandre Girard"]
publishDate = 2019-12-08T00:00:00+01:00
draft = false
book_title = "La mort immortelle"
book_author = "Liu Cixin"
book_lang = "fr"
+++

 

Quand Cheng Xin se réveilla, elle s’aperçut qu’elle était en apesanteur. Elle n’avait pas senti le temps passer. **Durant tout le processus, seule l’heure précédant l’entrée en hibernation et l’heure succédant au réveil étaient ressenties par l’hibernaute. Il n’avait aucune notion du temps passé en hibernation, et n’avait jamais l’impression que plus de deux heures s’étaient écoulées.** Aussi, à chaque réveil, le choc était violent, car l’hibernaute avait le sentiment d’avoir franchi une porte temporelle et d’être arrivé dans un autre monde.

Celui dans lequel se tenait désormais Cheng Xin consistait en un espace sphérique blanc. Elle vit qu’AA flottait non loin d’elle, vêtue de la même tenue d’hibernation moulante. Ses cheveux étaient mouillés et ses membres, ballants. De toute évidence, elle aussi venait juste de se réveiller. Leurs regards se croisèrent. Cheng Xin voulut dire quelque chose, mais l’engourdissement provoqué par le froid ne s’était pas encore dissipé et elle ne put produire aucun son. AA secoua vigoureusement la tête, signifiant qu’elle non plus ne savait rien.
