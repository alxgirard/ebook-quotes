+++
author = ["Alexandre Girard"]
publishDate = 2019-12-19T00:00:00+01:00
draft = false
book_title = "La mort immortelle"
book_author = "Liu Cixin"
book_lang = "fr"
+++

Cheng Xin s’efforça de se représenter ce paysage incroyable, mais en vain. Elle n’arrivait même pas à s’accrocher à un coin du tableau :

— Ce serait… trop fou ! Mais alors, **l’Univers deviendra-t-il un champ de ruines après la guerre ?** demanda Cheng Xin, trouvant rapidement une autre façon de formuler son interrogation : Les lois de la physique elles-mêmes deviendront-elles des ruines ?

— C’est peut-être déjà le cas… Les physiciens et les astronomes des nouveaux mondes s’efforcent de retrouver quel a pu être l’aspect original de ces lois avant le début de la guerre. Un modèle théorique assez précis a déjà été élaboré : il suggère qu’aux premiers temps de l’Univers, avant la guerre, le cosmos était un véritable jardin d’Éden, d’où l’appellation que l’on donne maintenant à cette époque, ancienne d’environ dix milliards d’années : l’Âge édénique de l’Univers. Bien entendu, cette beauté paradisiaque est uniquement exprimable en termes mathématiques ! Nous sommes incapables d’imaginer l’Univers à cette époque, les dimensions de notre cerveau ne suffisent pas.
