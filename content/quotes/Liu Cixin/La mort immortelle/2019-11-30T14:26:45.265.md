+++
author = ["Alexandre Girard"]
publishDate = 2019-11-30T00:00:00+01:00
draft = false
book_title = "La mort immortelle"
book_author = "Liu Cixin"
book_lang = "fr"
+++

Cette sphère blanche s’élargissait rapidement, ne laissant pas le temps à Cheng Xin de réfléchir davantage. Sa vitesse de dilatation était telle qu’elle semblait presque irréelle. Europe occupa bientôt la moitié du ciel et, d’une petite balle de ping-pong, elle devint rapidement une gigantesque planète. La sensation de “haut” et de “bas” fut instantanément ébranlée : Cheng Xin eut l’impression qu’Asie I s’apprêtait à sombrer dans ce monde blanc. Puis, cet astre de plus de trois mille kilomètres de diamètre fusa au-dessus de sa tête, recouvrant un instant le ciel tout entier. L’océan glacé d’Europe frôla alors les cités spatiales, et Cheng Xin put voir à sa surface des lignes entrecroisées, telles les empreintes digitales d’une énorme main blanche. Un vent puissant se leva dans l’atmosphère perturbée par la gravité d’Europe. Cheng Xin eut la sensation qu’une force invisible la tirait de gauche à droite. Sans ses chaussures magnétiques, elle aurait certainement été projetée hors du sol.

**Les objets qui n’étaient pas fixés au sol s’envolèrent, de même que quelques câbles reliés aux capsules spatiales, qui se mirent à danser dans les airs.** Un vrombissement terrifiant retentit sous ses pieds. C’était la réaction de la structure immense de la cité spatiale face à l’attraction d’Europe. Il ne fallut que trois minutes environ à Europe pour dépasser la cité spatiale et réapparaître de l’autre côté, en rétrécissant maintenant à toute vitesse. Au même moment, les huit cités des deux premières rangées allumèrent leurs propulseurs et réajustèrent leur position déréglée par l’attraction d’Europe. Huit boules de feu illuminèrent l’espace.

— Mon Dieu… à quelle distance est-elle passée ? demanda Cheng Xin, encore sous le choc.
