+++
author = ["Alexandre Girard"]
publishDate = 2019-10-23T00:00:00+02:00
draft = false
book_title = "La mort immortelle"
book_author = "Liu Cixin"
book_lang = "fr"
+++

— Mais nous sommes maintenant dans un espace à trois dimensions !

— Un espace quadridimensionnel englobe l’espace tridimensionnel, tout comme ce dernier englobe l’espace bidimensionnel. Pour vous donner une autre comparaison possible, **c’est comme si nous étions en ce moment même sur une feuille de papier tridimensionnelle dans un espace à quatre dimensions.**

Guan Yifan continua avec ferveur :
