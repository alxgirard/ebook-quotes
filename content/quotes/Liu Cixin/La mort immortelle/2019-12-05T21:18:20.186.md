+++
author = ["Alexandre Girard"]
publishDate = 2019-12-05T00:00:00+01:00
draft = false
book_title = "La mort immortelle"
book_author = "Liu Cixin"
book_lang = "fr"
+++

Elles effectuèrent le chemin du retour à bord du Halo. Après avoir confisqué toutes les propriétés du groupe, le Gouvernement fédéral avait redistribué une partie des richesses à Cheng Xin, environ le même capital que celui qu’elle possédait avant de donner les clefs du groupe à Thomas Wade. La somme était astronomique, mais elle restait modeste par rapport au chiffre d’affaires total du groupe Halo. Parmi les propriétés qui furent rendues à Cheng Xin se trouvait le vaisseau Halo. C’était déjà la troisième génération de cet appareil : un vaisseau interstellaire de petite taille où pouvaient prendre place deux ou trois passagers. L’écosystème autorégénératif à bord était remarquablement conçu et ressemblait à un merveilleux petit jardin.

Cheng Xin et AA voyagèrent sur tous les continents désormais dépeuplés de la planète. Elles survolèrent des forêts infinies, firent des randonnées à cheval dans des plaines désertes, marchèrent le long de plages abandonnées. La majorité des villes avaient été recouvertes par les lianes et une végétation touffue, et il ne restait plus que de rares poches d’habitation humaines. **Le nombre d’humains sur Terre était sensiblement le même qu’à la fin du Néolithique.**

Plus elles passaient du temps sur Terre, plus elles avaient l’illusion que la civilisation humaine n’avait été qu’un grand rêve.
