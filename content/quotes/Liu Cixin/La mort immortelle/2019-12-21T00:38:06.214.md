+++
author = ["Alexandre Girard"]
publishDate = 2019-12-21T00:00:00+01:00
draft = false
book_title = "La mort immortelle"
book_author = "Liu Cixin"
book_lang = "fr"
+++

— Je crois que ce sont les étoiles… répondit simplement Guan Yifan, tout aussi sidéré que Cheng Xin.

Dans l’espace, toutes les étoiles s’étaient changées en filaments lumineux. Ces formes effilées étaient familières à Cheng Xin. Elle avait déjà vu ce genre d’images sur des photographies en pause longue du ciel étoilé. Avec la rotation de la Terre, les étoiles sur ces photos devenaient des segments concentriques de même longueur, fuyant dans la même direction. Néanmoins, ici, **les étoiles étaient composées de segments de longueurs différentes, et aux directions divergentes. La plus longue des lignes traversait un tiers de l’espace.** De plus, elles se croisaient en différents angles, faisant paraître la nuit spatiale plus chaotique que jamais.

— Je crois que ce sont les étoiles, répéta Guan Yifan. Pour venir jusqu’ici, leur lumière doit traverser deux interfaces : tout d’abord celle entre la vitesse de la lumière standard et la vitesse de la lumière réduite puis, dans un second temps, l’horizon du trou noir.
